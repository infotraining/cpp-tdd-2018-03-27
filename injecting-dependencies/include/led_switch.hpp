#pragma once

#include "button.hpp"
#include "led_light.hpp"
#include <fstream>
#include <string>

class ILogger
{
public:
	virtual void log(const std::string& msg) = 0;
	virtual ~ILogger() = default;
};

class FileLogger : public ILogger
{
	std::ofstream log_file_;
public:
	FileLogger(const std::string& filename) : log_file_{filename.c_str()}
	{		
	}

	void log(const std::string& msg) override
	{
		log_file_ << "Log: " << msg << "\n";
	}
};

class LedSwitch : public ISwitch
{
	std::unique_ptr<ILed> led_;
	std::shared_ptr<ILogger> logger_;
 
public:
	explicit LedSwitch(std::unique_ptr<ILed> led)
		: led_{std::move(led)}
	{
	}

	void on() override
	{
		auto logger = get_logger();
		logger->log("Log: On called");
		led_->set_rgb(255, 255, 255);
	}

	void off() override
	{
		auto logger = get_logger();
		logger->log("Log: Off called");
		led_->set_rgb(0, 0, 0);
	}
protected:
	virtual std::shared_ptr<ILogger> get_logger()
	{
		if (!logger_)
			logger_ = std::make_shared<FileLogger>("log.dat");
		return logger_;
	}
};

namespace TemplateParameterInjection
{
	template <typename TLed = LEDLight>
	class LedSwitch : ISwitch
	{
		TLed& led_;
	public:
		explicit LedSwitch(TLed& led)
			: led_{ led }
		{
		}

		void on() override
		{
			led_.set_rgb(255, 255, 255);
		}

		void off() override
		{
			led_.set_rgb(0, 0, 0);
		}
	};

	namespace WithOwnership
	{
		template <typename TLed = LEDLight>
		class LedSwitch : ISwitch
		{
			TLed led_;
		public:
			explicit LedSwitch()				
			{
			}

			void on() override
			{
				led_.set_rgb(255, 255, 255);
			}

			void off() override
			{
				led_.set_rgb(0, 0, 0);
			}

			TLed& led()
			{
				return led_;
			}
		};
	}
}

