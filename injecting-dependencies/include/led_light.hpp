#ifndef LED_LIGHT_HPP
#define LED_LIGHT_HPP

#include <iostream>

class ILed
{
public:
	virtual void set_rgb(size_t r, size_t g, size_t b) = 0;
	virtual ~ILed() = default;
};

class LEDLight : public ILed
{
public:
    void set_rgb(size_t r, size_t g, size_t b) override
    {
        std::cout << "Setting(" << r << ", " << g << ", " << b << ")\n";
    }
};

#endif //LED_LIGHT_HPP
