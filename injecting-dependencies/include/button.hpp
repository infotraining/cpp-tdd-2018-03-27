#ifndef BUTTON_HPP
#define BUTTON_HPP

#include "led_light.hpp"
#include <memory>

class ISwitch
{
public:
	virtual void on() = 0;
	virtual void off() = 0;
	virtual ~ISwitch() = default;
};

class Button
{
    std::unique_ptr<ISwitch> led_;
    bool is_on_;
public:
    Button(std::unique_ptr<ISwitch> led_switch) :
		led_{ move(led_switch) }, is_on_{ false }
    {}

    void click() 
    {
        if (!is_on_)
        {
			led_->on();
            is_on_ = true;
        }
        else
        {
            led_->off();
            is_on_ = false;
        }
    }
};

#endif