#include "boost/di.hpp"
#include "button.hpp"
#include "led_switch.hpp"
#include <iostream>

using namespace std;
namespace di = boost::di;

void handwired_dependency()
{
    Button btn(make_unique<LedSwitch>(make_unique<LEDLight>()));

    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();
}

int main()
{
    const auto injector = di::make_injector(
        di::bind<ISwitch>().to<LedSwitch>(),
        di::bind<ILed>().to<LEDLight>());

    auto btn = injector.create<Button>();

    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();

    cout << "\n\n";

    auto ptr_btn = injector.create<unique_ptr<Button>>();
    ptr_btn->click();

    system("PAUSE");

    return 0;
}