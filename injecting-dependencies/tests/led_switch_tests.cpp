#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "led_switch.hpp"
#include "led_light.hpp"
#include <memory>

using namespace std;

class MockLed : public ILed
{
public:
	MOCK_METHOD3(set_rgb, void (size_t, size_t, size_t));
};

class MockLogger : public ILogger
{
public:
	MOCK_METHOD1(log, void (const std::string&));
};

class TestableLedSwitch : public LedSwitch
{
	shared_ptr<MockLogger> mq_logger_;
public:
	TestableLedSwitch(unique_ptr<ILed> led) 
		: LedSwitch{move(led)},  mq_logger_{ make_shared<MockLogger>() }
	{}

	MockLogger& get_mq_logger()
	{
		return *mq_logger_;
	}

protected:
	std::shared_ptr<ILogger> get_logger() override
	{
		return mq_logger_;
	}
};

TEST(LedSwitchTests, On_SetsRGBToFFF)
{
	// Arrange
	auto mq_led = make_unique<MockLed>();
	EXPECT_CALL(*mq_led, set_rgb(255, 255, 255)).Times(1);
	
	TestableLedSwitch led_switch{ move(mq_led) };

	// Act
	led_switch.on();
}

TEST(LedSwitchTests, Off_SetsRGBToFFF)
{
	// Arrange
	auto mq_led = make_unique<MockLed>();
	EXPECT_CALL(*mq_led, set_rgb(0, 0, 0)).Times(1);

	TestableLedSwitch led_switch{ move(mq_led) };

	// Act
	led_switch.off();
}

TEST(LedSwitchTests, On_LogIsSaved)
{
	using namespace ::testing;

	auto mq_led = make_unique<NiceMock<MockLed>>();

	TestableLedSwitch led_switch{ move(mq_led) };

	EXPECT_CALL(led_switch.get_mq_logger(), log(StartsWith("Log")));

	led_switch.on();
}

namespace TemplateParameterInjection
{
	struct MockLed
	{
		MOCK_METHOD3(set_rgb, void(size_t, size_t, size_t));
	};

	TEST(LedSwitchTemplatedTests, On_SetsRGBToFFF)
	{
		MockLed mq_led_switch;
		LedSwitch<MockLed> led_switch(mq_led_switch);

		EXPECT_CALL(mq_led_switch, set_rgb(255, 255, 255));

		led_switch.on();
	}

	namespace WithOwnership
	{
		TEST(LedSwitchTemplatedTests2, On_SetsRGBToFFF)
		{
			LedSwitch<MockLed> led_switch;

			EXPECT_CALL(led_switch.led(), set_rgb(255, 255, 255));

			led_switch.on();
		}
	}
}