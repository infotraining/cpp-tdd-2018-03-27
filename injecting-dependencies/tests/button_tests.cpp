#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "button.hpp"

using namespace std;

class MockSwitch : public ISwitch
{
public:
	MOCK_METHOD0(on, void ());
	MOCK_METHOD0(off, void ());
};

TEST(ButtonTests_OffState, TurnsLedOn)
{
	// Arrange
	auto mq_led = make_unique<MockSwitch>();
	EXPECT_CALL(*mq_led, on()).Times(1);
	Button btn{ move(mq_led) };

	// Act
	btn.click();
}

TEST(ButtonTests_OnState, TurnsLedOff)
{
	using namespace ::testing;

	auto mq_led = make_unique<NiceMock<MockSwitch>>();	
	MockSwitch& ref_mq_led = *mq_led;

	Button btn{ move(mq_led) };
	btn.click();

	EXPECT_CALL(ref_mq_led, off());
	btn.click();
}