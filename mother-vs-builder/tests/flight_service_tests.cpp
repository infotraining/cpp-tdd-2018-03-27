#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <memory>

#include "flight_model.hpp"
#include "flight_repository.hpp"
#include "flight_service.hpp"

using namespace std;

struct Mother
{
    constexpr static const char* flight_no = "LOT101";
    constexpr static const char* client = "John Newman";
    constexpr static const char* timestamp = "2017/01/01 1:45am";

    static ReservationRequest create_reservation_request()
    {
        return ReservationRequest{Flight{flight_no, 100.0}, client, timestamp};
    }
};

class ReservationRequestBuilder
{
    constexpr static const char* flight_no = "LOT101";
    constexpr static const char* client = "John Newman";
    constexpr static const char* timestamp = "2017/01/01 1:45am";

    ReservationRequest reservation_request_{Flight{flight_no, 100.0}, client, timestamp};

public:
    ReservationRequestBuilder() = default;

    ReservationRequestBuilder& with_client(const string& client)
    {
        reservation_request_.client = client;

        return *this;
    }

    ReservationRequestBuilder& with_timestamp(const string& timestamp)
    {
        reservation_request_.timestamp = timestamp;

        return *this;
    }

    ReservationRequestBuilder& with_flight(const Flight& flight)
    {
        reservation_request_.flight = flight;

        return *this;
    }

    ReservationRequest get_reservation_request() const
    {
        return reservation_request_;
    }
};

class MockFlightRepository : public FlightRepository
{
public:
    MOCK_METHOD1(add, void(const Flight&));
};

class FlightServiceTests : public ::testing::Test
{
protected:
    MockFlightRepository flight_repository_;
    FlightReservationService sut_;

public:
    FlightServiceTests()
        : sut_{flight_repository_}
    {
    }
};

TEST_F(FlightServiceTests, CanAddReservationToRepository)
{
    auto reservation_request = Mother::create_reservation_request();

    EXPECT_CALL(flight_repository_, add(reservation_request.flight)).Times(1);

    sut_.make_reservation(reservation_request);
}

TEST_F(FlightServiceTests, ThrowsWhenTimestampInInvalidFormat)
{
    ReservationRequestBuilder reservation_request_builder;
    reservation_request_builder.with_timestamp("2017|01|01 1:45am");
    auto reservation_request = reservation_request_builder.get_reservation_request();

    EXPECT_THROW(sut_.make_reservation(reservation_request), std::invalid_argument);
}

int foo(int x)
{
	
}

int foo(int& x)
{
	
}

string get_name()
{
	return "Jan Kowalski";
}

void print_name(const string& name)
{
	cout << name << endl;
}

vector<string> name_index;

void save_in_container(const string& name)
{
	name_index.push_back(name);
}

void save_in_container(string&& name)
{
	name_index.push_back(move(name));
}

void save_all_to_container(string n1, string n2, string n3)
{
	name_index.push_back(move(n1));
	name_index.push_back(move(n2));
	name_index.push_back(move(n3));
}

template <typename T>
void save_all_with_forwarding(T&& n1, T&& n2, T&& n3)
{
	name_index.push_back(forward<T>(n1));
	name_index.push_back(forward<T>(n2));
	name_index.push_back(forward<T>(n3));
}

TEST(ParamsTests, ByConstRef)
{
	string name = "Jan";

	print_name(name);
	print_name(get_name());

	save_in_container(name); // const string&
	save_in_container(move(name)); // string&&
	save_in_container(get_name()); // string&&

	save_all_to_container(get_name(), name, get_name());
	save_all_with_forwarding(get_name(), name, get_name());
}

namespace LegacyCode
{
	void get_data(string& s, int& n, double& d)
	{
		
	}
}

tuple<string, int, double> get_data()
{
	string text = "text";
	return make_tuple(move(text), 42, 3.14);
}

TEST(PassingParamsTests, Functions)
{
	//auto[text, n, d] = get_data();
	auto result = get_data();

	ASSERT_EQ("text"s, get<0>(result));

	string text;
	int n;
	double d;

	tie(text, n, d) = get_data();

	LegacyCode::get_data(text, n, d);
}
