#include <algorithm>
#include <array>

#include "bowling_game.hpp"
#include "gtest/gtest.h"
#include <gmock/gmock-matchers.h>

using namespace std;



class BowlingGameTests : public ::testing::Test
{
protected:
	BowlingGame game;

	void roll_many(size_t no_of_rolls, size_t pins)
	{
		for (size_t i = 0; i < no_of_rolls; ++i)
			game.roll(pins);
	}

	void roll_spare()
	{
		game.roll(9);
		game.roll(1);
	}

	void roll_strike()
	{
		game.roll(10);
	}
};

TEST_F(BowlingGameTests, WhenGameStartsScoreIsZero)
{	
	ASSERT_EQ(0, game.score());
}

TEST_F(BowlingGameTests, WhenAllRollsInGutterScoreIsZero)
{	
	roll_many(20, 0);

	ASSERT_EQ(0, game.score());
}

TEST_F(BowlingGameTests, WhenAllRollsNoMarkScoreIsSumOfPins)
{
	roll_many(20, 2);

	ASSERT_EQ(40, game.score());
}

TEST_F(BowlingGameTests, WhenSpareNextRollIsCountedTwice)
{	
	roll_spare();
	roll_many(18, 1);

	ASSERT_EQ(29, game.score());
}

TEST_F(BowlingGameTests, WhenStrikeTwoNextRollsAreCountedTwice)
{
	roll_strike();
	roll_many(18, 1);

	ASSERT_EQ(30, game.score());
}

TEST_F(BowlingGameTests, PerfectGameScores300)
{
	for (int i = 0; i < 12; ++i)
		roll_strike();

	ASSERT_EQ(300, game.score());
}

TEST_F(BowlingGameTests, WhenSpareInLastFrameBonusExtraRoll)
{
	roll_many(18, 1);
	roll_spare();
	game.roll(6);

	ASSERT_EQ(34, game.score());
}

TEST_F(BowlingGameTests, WhenStrikeInLastFrameBonusTwoExtraRolls)
{
	roll_many(18, 1);
	roll_strike();
	game.roll(6);
	game.roll(7);

	ASSERT_EQ(41, game.score());
}

struct BowlingTestsParams
{
	std::initializer_list<size_t> pins;
	size_t expected_score;
};

ostream& operator<<(ostream& out, const BowlingTestsParams& p)
{
	out << "{[ ";
	for (const auto& pins : p.pins)
		out << pins << " ";
	out << " ], " << p.expected_score << "}";
	return out;
}

struct BowlingBulkTests : ::testing::TestWithParam<BowlingTestsParams>
{};

TEST_P(BowlingBulkTests, GameScore)
{
	BowlingGame game;

	BowlingTestsParams param = GetParam();

	for (const auto& p : param.pins)
		game.roll(p);

	ASSERT_EQ(param.expected_score, game.score());
}

BowlingTestsParams bowling_test_params[] = {
	{ { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, 20},
	{ { 10, 3, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, 44 },
	{ { 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 10 }, 119 },
	{ { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }, 300 }
};

INSTANTIATE_TEST_CASE_P(PackOfBowlingTests, BowlingBulkTests, ::testing::ValuesIn(bowling_test_params));


//TEST("Bowling game")
//{
//	BowlingGame game;
//
//	SECTION("WhenGameStartsScoreIsZero")
//	{
//		REQUIRE(game.score() == 0);
//	}
//
//	SECTION("WhenAllRollsInGutterScoreIsZero")
//	{
//		for (int i = 0; i < 20; ++i)
//			game.roll(0);
//
//		REQUIRE(game.score() == 0);
//	}
//}

class VectorTests : public ::testing::Test
{
protected:
	vector<int> data;
};


class EmptyVectorTests : public VectorTests
{	
};

TEST_F(EmptyVectorTests, DISABLED_WhenEmptySizeIsZero)
{
	ASSERT_EQ(0, data.size());
}

class VectorWithItemsTests : public VectorTests
{
protected:
	void SetUp() override
	{
		data.push_back(10);
		data.push_back(11);

		ASSERT_EQ(2, data.size());
	}	
};

TEST_F(VectorWithItemsTests, DISABLED_PushBackChangesSize)
{
	data.push_back(5);

	ASSERT_EQ(3, data.size());
}


//// catch
//TEST("vector", "[vector][push_back]")
//{
//	
//}