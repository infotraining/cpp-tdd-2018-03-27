#ifndef BOWLING_GAME_HPP
#define BOWLING_GAME_HPP

#include <array>

class BowlingGame
{
	std::array<size_t, 21> pins_ = {};
	size_t roll_no_{};

	constexpr static size_t pins_in_frame = 10;
	constexpr static size_t frame_no = 10;
public:
	bool is_strike(size_t roll_index) const
	{
		return pins_[roll_index] == pins_in_frame;
	}

	size_t strike_bonus(size_t roll_index) const
	{
		return pins_[roll_index + 1] + pins_[roll_index + 2];
	}

	size_t score() const
	{
		size_t result{};

		size_t roll_index = 0;

		for (size_t i = 0; i < frame_no; ++i)
		{
			if (is_strike(roll_index))
			{
				result += pins_in_frame + strike_bonus(roll_index);
				++roll_index;
			}
			else
			{
				result += frame_score(roll_index);
				if (is_spare(roll_index))
				{
					result += spare_bonus(roll_index);
				}
				roll_index += 2;
			}
		}

		return result;
	}

	void roll(size_t pins)
	{
		pins_[roll_no_] = pins;
		++roll_no_;
	}

	bool is_spare(size_t roll_index) const
	{
		return frame_score(roll_index) == pins_in_frame;
	}

	size_t spare_bonus(size_t roll_index) const
	{
		return pins_[roll_index + 2];
	}

	size_t frame_score(size_t roll_index) const
	{
		return  pins_[roll_index] + pins_[roll_index + 1];
	}
};

#endif

