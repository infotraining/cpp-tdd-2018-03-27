#include <algorithm>

#include "data.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <memory>

using namespace std;

class Interface
{
public:
	virtual ~Interface() = default;

	virtual std::string get_name() const = 0;
	virtual std::string get_value(int x) = 0;
	virtual std::string get_value(int x) const = 0;
	virtual bool save_value(int k, const string& v) = 0;
	virtual std::vector<int> get_data(const std::string& filename) const = 0;
	virtual int generate() = 0;
};

struct MockInterface : Interface
{
	MOCK_CONST_METHOD0(get_name, std::string ());
	MOCK_METHOD1(get_value, std::string (int));
	MOCK_CONST_METHOD1(get_value, std::string (int));
	MOCK_METHOD2(save_value, bool (int, const string&));
	MOCK_CONST_METHOD1(get_data, std::vector<int> (const std::string&));
	MOCK_METHOD0(generate, int ());
};

TEST(DefaultValuesTests, ReturningDefaultValues)
{
	testing::NiceMock<MockInterface> mq;

	ASSERT_EQ(0, mq.generate());
	ASSERT_EQ(""s, mq.get_name());
	ASSERT_THAT(mq.get_data("text"), ::testing::IsEmpty());
}


struct GmockDefaultValuesTests : ::testing::Test
{
protected:
	testing::NiceMock<MockInterface> mq;

	void SetUp() override
	{
		::testing::DefaultValue<int>::Set(665);
		::testing::DefaultValue<string>::Set("data");
	}
	void TearDown() override
	{
		::testing::DefaultValue<int>::Clear();
		::testing::DefaultValue<string>::Clear();
	}
};

TEST_F(GmockDefaultValuesTests, DefaultValuesSetImlicitlyForFixture)
{
	using namespace ::testing;	

	ASSERT_EQ(665, mq.generate()); // default
	ASSERT_EQ("data"s, mq.get_name());
}

TEST_F(GmockDefaultValuesTests, DefaultValuesSetExplicitly)
{
	using namespace ::testing;

	ON_CALL(mq, generate()).WillByDefault(Return(42));
	ASSERT_EQ(42, mq.generate()); // explicitly set for generate()

	vector<int> data = { 1, 2, 3 };
	ON_CALL(mq, get_data("nagrody_prm")).WillByDefault(Return(data));	

	ASSERT_THAT(mq.get_data("nagrody_prm"), ElementsAre(1, 2, 3));
}

TEST(GMockExpectCallTests, SettingExpectation)
{
	using namespace ::testing;

	MockInterface mq;

	EXPECT_CALL(mq, get_name()).WillOnce(Return("Jan"));

	ASSERT_EQ(mq.get_name(), "Jan");
}

TEST(GMockExpectCallTests, SettingExpectationWithHowManyTimes)
{
	using namespace ::testing;

	MockInterface mq;
	
	EXPECT_CALL(mq, save_value(Gt(0), _)).Times(AtMost(3)).WillRepeatedly(Return(true)).RetiresOnSaturation();
	EXPECT_CALL(mq, save_value(3, "a")).WillRepeatedly(Return(false));
	
	ASSERT_TRUE(mq.save_value(1, "a")) << "1";
	ASSERT_TRUE(mq.save_value(2, "b")) << "2";
	ASSERT_FALSE(mq.save_value(3, "a")) << "3";
	ASSERT_TRUE(mq.save_value(4, "b")) << "4";
}

TEST(GMockExpectCallTests, ReturnDifferentValuesBasedOnArgs)
{
	using namespace ::testing;

	MockInterface mq;

	EXPECT_CALL(mq, get_value(Gt(0))).WillRepeatedly(Return("positive"));
	EXPECT_CALL(mq, get_value(Lt(0))).WillRepeatedly(Return("negative"));

	ASSERT_EQ("positive", mq.get_value(20));
	ASSERT_EQ("negative", mq.get_value(-20));
	//mq.get_value(0);
}

TEST(GMockSequencedCalls, ExpectingCallsInSequence)
{
	using namespace ::testing;

	MockInterface mq;

	vector<string> names = { "Jan", "Kowalski" };
	int index{};

	InSequence s;

	EXPECT_CALL(mq, get_name()).Times(2).WillRepeatedly(Invoke([&] { return names[index++]; }));
	EXPECT_CALL(mq, generate()).WillOnce(Return(665));
	EXPECT_CALL(mq, save_value(665, "Jan Kowalski"));
	
	auto name1 = mq.get_name();
	auto name2 = mq.get_name();	
	auto key = mq.generate();
	mq.save_value(key, name1 + " " + name2);
}

//TEST(GMockDemoTests, UsingMockAsSpy)
//{
//	using namespace ::testing;
//
//	MockInterface mq;
//
//	vector<int> spy;
//
//	EXPECT_CALL(mq, get_value(_))
//		.WillRepeatedly(Invoke([&](int arg) { spy.push_back(arg); return to_string(arg); }));
//
//	ASSERT_EQ(mq.get_value(1), StrEq("1"));
//	ASSERT_THAT(mq.get_value(2), StrEq("2"));
//	ASSERT_THAT(mq.get_value(3), StrEq("3"));
//
//	ASSERT_THAT(spy, ElementsAre(1, 2, 3));
//}

TEST(GMockDemoTests, ThrowingExceptions)
{	
	using namespace ::testing;

	MockInterface mq;

	invalid_argument excpt("Invalid arg");

	EXPECT_CALL(mq, get_value(AllOf(Lt(0), Ge(-5)))).WillRepeatedly(Throw(excpt));

	ASSERT_THROW(mq.get_value(-2), invalid_argument);
}

////////////////////////////////////////////
// Move Semantics

struct Gadget
{
	int id;

	explicit Gadget(int id)
		: id{id}
	{
	}

	virtual string description()
	{
		return "Gadget: " + to_string(id);
	}
};

class MoveSemantics
{
public:
	virtual unique_ptr<Gadget> create_gadget(int id) = 0;
	virtual void sink(unique_ptr<Gadget> g) = 0;
	virtual ~MoveSemantics() = default;
};

struct MockMoveSemantics : MoveSemantics
{
	MOCK_METHOD1(create_gadget_, Gadget*(int));

	unique_ptr<Gadget> create_gadget(int id) override
	{
		return unique_ptr<Gadget>(create_gadget_(id));
	}

	MOCK_METHOD1(sink_, void(Gadget*));

	void sink(unique_ptr<Gadget> g) override
	{
		sink_(g.get());
	}
};

struct MockMoveSemantics2 : MoveSemantics
{
	MOCK_METHOD1(create_gadget, unique_ptr<Gadget> (int));
	
	MOCK_METHOD1(sink_, void(Gadget*));

	void sink(unique_ptr<Gadget> g) override
	{
		sink_(g.get());
	}
};

void using_gadget(MoveSemantics& ms)
{
	auto g = ms.create_gadget(1);
	ms.sink(move(g));
}

TEST(GMockMoveSemantics, MocksWithMove)
{
	using namespace ::testing;

	MockMoveSemantics mq;

	EXPECT_CALL(mq, create_gadget_(_)).WillOnce(Invoke([](int id) { return new Gadget(id); }));
	//EXPECT_CALL(mq, sink_(Field(&Gadget::id, Eq(1)))).Times(1);

	using_gadget(mq);
}

TEST(GMockMoveSemantics, MocksWithMove2)
{
	using namespace ::testing;

	MockMoveSemantics2 mq;

	auto g = make_unique<Gadget>(1);

	EXPECT_CALL(mq, create_gadget(_)).WillOnce(Return(ByMove(move(g))));
	//EXPECT_CALL(mq, sink_(Field(&Gadget::id, Eq(1)))).Times(1);

	using_gadget(mq);
}