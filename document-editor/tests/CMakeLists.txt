set(PROJECT_TEST_NAME ${PROJECT_NAME_STR}_tests)

set(TEST_CURRENT_DIR ${PROJECT_SOURCE_DIR}/tests)

# Prepare "Catch" & "Trompeloeil" libraries for other executables
set(CATCH_INCLUDE_DIR ${TEST_CURRENT_DIR}/catch)
set(TROMPELOEIL_INCLUDE_DIR ${TEST_CURRENT_DIR}/trompeloeil)

add_library(Catch INTERFACE)
add_library(Trompeloeil INTERFACE)

target_include_directories(Catch INTERFACE ${CATCH_INCLUDE_DIR})
target_include_directories(Trompeloeil INTERFACE ${TROMPELOEIL_INCLUDE_DIR})

# unit tests files
file(GLOB TEST_SOURCES ${TEST_CURRENT_DIR}/*_tests.cpp)
add_executable(${PROJECT_TEST_NAME} ${TEST_SOURCES})
target_link_libraries(${PROJECT_TEST_NAME} Catch Trompeloeil Di ${PROJECT_LIB_NAME})

add_test(tests ${PROJECT_TEST_NAME})