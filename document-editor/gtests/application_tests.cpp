#include "gmock/gmock.h"

#include "application.hpp"
#include "mocks/mock_console.hpp"
#include "mocks/mock_command.hpp"

using namespace ::testing;



struct ApplicationTests : Test
{
	NiceMock<MockConsole> mq_console;
	std::shared_ptr<MockCommand> mq_cmd;
	Application app;


	ApplicationTests() : mq_cmd{ std::make_shared<MockCommand>() }, app{ mq_console }
	{
	}

protected:
	void SetUp() override
	{
		app.add_command("Cmd", mq_cmd);
	}

	void prepare_for_exit()
	{
		EXPECT_CALL(mq_console, get_line()).WillOnce(Return("Exit"));
	}
};

struct ApplicationTests_MainLoop : ApplicationTests
{};

TEST_F(ApplicationTests_MainLoop, ShowsPrompt)
{
	EXPECT_CALL(mq_console, print("Enter command:")).Times(1);
	prepare_for_exit();

	app.run();
}

TEST_F(ApplicationTests_MainLoop, GetsLineFromInput)
{	
	EXPECT_CALL(mq_console, get_line()).WillOnce(Return("Exit"));

	app.run();
}

TEST_F(ApplicationTests_MainLoop, ExecutesCommands)
{
	InSequence s;
	EXPECT_CALL(mq_console, get_line()).WillOnce(Return("Cmd"));
	EXPECT_CALL(*mq_cmd, execute()).Times(1);
	prepare_for_exit();

	app.run();
}



struct CommandTests : ::testing::Test
{		
};

struct PrintCmdTests : CommandTests
{	
	MockConsole mq_console;
	Document doc;
	PrintCmd print_cmd;

	PrintCmdTests() : doc{ "abc" }, print_cmd{ doc, mq_console }
	{
	}
};

TEST_F(PrintCmdTests, Execute_PrintsDocContentOnConsole)
{
	EXPECT_CALL(mq_console, print("[abc]")).Times(1);

	print_cmd.execute();
}