#ifndef MOCK_COMMAND_HPP
#define MOCK_COMMAND_HPP

#include "gmock/gmock.h"
#include "command.hpp"

class MockCommand : public Command
{
public:
	MOCK_METHOD0(execute, void());
};



#endif // MOCK_COMMAND_HPP
