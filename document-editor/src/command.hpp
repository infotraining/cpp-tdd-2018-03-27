#ifndef COMMAND_HPP
#define COMMAND_HPP

#include "clipboard.hpp"
#include "console.hpp"
#include "document.hpp"
#include <memory>
#include <stack>

class Command
{
public:
	virtual void execute() = 0;
	virtual ~Command() = default;
};

class PrintCmd : public Command
{
	Document& doc_;
	Console& console_;
public:

	PrintCmd(Document& doc, Console& console)
		: doc_{ doc },
		console_{ console }
	{
	}

	void execute() override
	{
		console_.print("[" + doc_.text() + "]");
	}
};


#endif // COMMAND_HPP
