#ifndef DOCUMENT_HPP
#define DOCUMENT_HPP

#include <sstream>
#include <string>
#include <algorithm>
#include <cctype>

class Document
{
    std::string text_;

public:
    class Memento
    {
    private:
        std::string snapshot_;

        friend class Document;
    };

    Document() : text_{}
    {
    }

    Document(const std::string& text) : text_{text}
    {
    }

    std::string text() const
    {
        return text_;
    }

    size_t length() const
    {
        return text_.size();
    }

    void add_text(const std::string& txt)
    {
        text_ += txt;
    }

    void to_upper()
    {
		std::transform(text_.begin(), text_.end(), text_.begin(), 
			[](int c) { return std::toupper(c); });
    }

    void to_lower()
    {
		std::transform(text_.begin(), text_.end(), text_.begin(),
			[](int c) { return std::tolower(c); });
    }

    void clear()
    {
        text_.clear();
    }

    Memento create_memento() const
    {
        Memento memento;
		memento.snapshot_ = text_;

        return memento;
    }

    void set_memento(Memento& memento)
    {
		text_ = memento.snapshot_;
    }

    void replace(size_t start_pos, size_t count, const std::string& text)
    {
        text_.replace(start_pos, count, text);
    }
};

#endif
