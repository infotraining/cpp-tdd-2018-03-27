#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <unordered_map>

#include "console.hpp"
#include "command.hpp"

namespace Commands
{
	constexpr auto cmd_exit = "Exit";
}

class Application
{
	Console& console_;
	std::unordered_map<std::string, std::shared_ptr<Command>> cmds_;

public:
	explicit Application(Console& console)
		: console_{ console }
	{
	}

	void run()
	{
		while (true)
		{
			console_.print("Enter command:");

			auto cmd_text = console_.get_line();

			if (cmd_text == Commands::cmd_exit)
				break;

			cmds_.at(cmd_text)->execute();
		}
	}

	void add_command(const std::string& cmd_name, std::shared_ptr<Command> cmd)
	{
		cmds_.insert(std::make_pair(cmd_name, cmd));
	}
};

#endif // APPLICATION_HPP


