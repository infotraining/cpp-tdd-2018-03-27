#include <iostream>

#include "application.hpp"
#include "command.hpp"
#include "document.hpp"
#include "di.hpp"

using namespace std;
namespace di = boost::di;

int main()
{
	const auto injector = di::make_injector(
		di::bind<Console>().to<Terminal>()
	);

	auto app = injector.create<Application>();

	app.add_command("Print"s, injector.create<shared_ptr<PrintCmd>>());

	app.run();
}
